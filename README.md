# **SimpleChat** #

Simple Chat application, based on Vertx framework and WebSocket for two-way communication. Java 8 required on server.


## How to Build ##

### **Usage** ###

**Building:**

```
#!shell

mvn package
```


Run command above in the project's root directory

**Executing:**

Run executable jar file SimpleChat-fat.jar in /target directory


```
#!shell

java -jar SimpleChat-fat.jar
```
import java.io.IOException;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.text.SimpleDateFormat;

import io.vertx.core.AbstractVerticle;
import io.vertx.ext.web.Router;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.buffer.Buffer;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class WebserverVerticle extends AbstractVerticle {

	@Override
	public void start() {
		final Pattern chatUrlPattern = Pattern.compile("/simplechat/?");
		final EventBus eventBus = vertx.eventBus();


		Router router = Router.router(vertx);
		router.route("/simplechat").handler(routingContext -> {
			routingContext.response().sendFile("web/index.html");
			System.out.println("LOG: Requested page has been sent");
		});

		vertx.createHttpServer().requestHandler(router::accept).listen(8080);

		vertx.createHttpServer().websocketHandler( ws -> {
			final Matcher matcher = chatUrlPattern.matcher(ws.path());
			if (!matcher.matches()) {
				ws.reject();
				return;
			}
			final String id = ws.textHandlerID();
			vertx.sharedData().getLocalMap("users").put(id, id);
			System.out.println("LOG: Websocket connection established with: "+id);
			ws.closeHandler( (Void event) -> {
				vertx.sharedData().getLocalMap("users").remove(id);
				System.out.println("LOG: WebSocket connection closed with: "+ id);
			});
			ws.handler( (Buffer data) -> {
				System.out.println("LOG: Received incoming message");
				ObjectMapper m = new ObjectMapper();
				try {
					System.out.println("LOG: Forwarding message to all users...");
					JsonNode rootNode = m.readTree(data.toString());
					SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yy HH:mm:ss");
					((ObjectNode) rootNode).put("date", formatter.format(new Date()));
					((ObjectNode) rootNode).put("senderip", ws.remoteAddress().host());

					String jsonOutput = m.writeValueAsString(rootNode);
					for (Object user : vertx.sharedData().getLocalMap("users").values()) {
						eventBus.send((String) user, jsonOutput);
						System.out.println("LOG: Message sent to: "+ user);
					}

				} catch (IOException e) {
					System.out.println("LOG: Exception occurred in WebSocket handler. WebSocket connection closed");
					ws.close();
				}

			});

		}).listen(8080);
	}
}
import io.vertx.core.Vertx;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import io.vertx.core.json.JsonObject;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.concurrent.TimeUnit;
import java.nio.channels.ClosedChannelException;
import io.vertx.core.buffer.Buffer;
import io.netty.handler.codec.http.websocketx.WebSocketHandshakeException;
import java.lang.Throwable;

@RunWith(VertxUnitRunner.class)
public class WebserverVerticleTest {

    private Vertx vertx;

    @Before
    public void setUp() throws Exception{
        CountDownLatch latch = new CountDownLatch(1);
        vertx = Vertx.vertx();
        vertx.deployVerticle(new WebserverVerticle(), res -> {
            latch.countDown();
        });
        latch.await();
    }
    @Test
    public void testPage(TestContext context) throws Exception {
	    CountDownLatch latch = new CountDownLatch(1);
        vertx.createHttpClient().getNow(8080, "localhost", "/simplechat/", response -> {
            context.assertEquals(response.statusCode(), 200);
            context.assertEquals(response.headers().get("content-type"), "text/html");
			latch.countDown();
		});
		latch.await();
    }
	@Test
	public void testPageFail(TestContext context) throws Exception {
		CountDownLatch latch = new CountDownLatch(1);
        vertx.createHttpClient().getNow(8080, "localhost", "/difficultchat/", response -> {
            context.assertEquals(response.statusCode(), 404);
			latch.countDown();
        });
		latch.await();
    }	
	@Test
	public void testWebSocket(TestContext context) throws Exception{
		CountDownLatch latch = new CountDownLatch(1);
		vertx.createHttpClient().websocket(8080, "localhost", "/simplechat/", websocket -> {
			websocket.handler(data -> {
				try {
					ObjectMapper mapper = new ObjectMapper();
					JsonNode jnode = mapper.readTree(data.toString());
					context.assertEquals("\"12345 qwerty йцукен\"",jnode.get("message").toString());
					context.assertFalse(jnode.get("date").toString().equals(""));
					context.assertTrue(jnode.get("senderip").toString().equals("\""+ websocket.localAddress().host()+"\""));
					latch.countDown();
				} catch (IOException e) {
					websocket.resume();
				}
			});
			JsonObject json = new JsonObject();
			json.put("message", "12345 qwerty йцукен");
			json.put("date", "");
			json.put("senderip", "");
			websocket.writeFinalTextFrame(json.toString());
		});
		latch.await();
	}
	@Test
	public void testWebSocketSpecial(TestContext context) throws Exception{
			CountDownLatch latch = new CountDownLatch(1);
			vertx.createHttpClient().websocket(8080, "localhost", "/simplechat/", websocket -> {
			websocket.handler(data -> {
				try {
					ObjectMapper mapper = new ObjectMapper();
					JsonNode jnode = mapper.readTree(data.toString());
					context.assertEquals("\"123\\nqwer'\\\":/йцукен\"",jnode.get("message").toString());
					context.assertFalse(jnode.get("date").toString().equals(""));
					context.assertTrue(jnode.get("senderip").toString().equals("\""+ websocket.localAddress().host()+"\""));
					latch.countDown();
				} catch (IOException e) {
					websocket.resume();
				}
			});
			JsonObject json = new JsonObject();
			json.put("message", "123\nqwer'\":/йцукен");
			json.put("date", "");
			json.put("senderip", "");
			websocket.writeFinalTextFrame(json.toString());
		});
		latch.await();
	}
	

	@Test
	public void testWebSocketMultiUser(TestContext context) throws Exception{
		CountDownLatch latch = new CountDownLatch(1);
		vertx.createHttpClient().websocket(8080, "localhost", "/simplechat/", websocket -> {
			websocket.handler(data -> {
				try {
					ObjectMapper mapper = new ObjectMapper();
					JsonNode jnode = mapper.readTree(data.toString());
					context.assertEquals("\"12345 qwerty йцукен\"",jnode.get("message").toString());
					context.assertFalse(jnode.get("date").toString().equals(""));
					context.assertTrue(jnode.get("senderip").toString().equals("\""+ websocket.localAddress().host()+"\""));
					latch.countDown();
				} catch (IOException e) {
					websocket.resume();
				}
			});
			JsonObject json = new JsonObject();
			json.put("message", "12345 qwerty йцукен");
			json.put("date", "");
			json.put("senderip", "");
			websocket.writeFinalTextFrame(json.toString());
		});
		//readonly user
		vertx.createHttpClient().websocket(8080, "localhost", "/simplechat/", websocket -> {
			websocket.handler(data -> {
				try {
					ObjectMapper mapper = new ObjectMapper();
					JsonNode jnode = mapper.readTree(data.toString());
					context.assertEquals("\"12345 qwerty йцукен\"",jnode.get("message").toString());
					context.assertFalse(jnode.get("date").toString().equals(""));
					context.assertFalse(jnode.get("senderip").toString().equals(""));
					latch.countDown();
				} catch (IOException e) {
					websocket.resume();
				}
			});
		});
		latch.await();
	}
    @After
    public void tearDown() {
        vertx.close();
    }
}